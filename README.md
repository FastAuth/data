# data

Programs related to the extraction of information from Medical Clinical Policy
Bulletins.

## insurance companies

- `Aetna`
- `IBX`

## organization

Files related to insurance company `X` should be placed in `data/X/`.

Programs and scripts that deal with extracting information from multiple
companies should be placed in `data/generic`

## flow

The general idea is to extract all text from a web page and use that text
to extract the required information. The text can (and probably should) be 
converted to an intermediate representation to make subsequent parsing easier.

The parser returns a `CPBInfo` object which is then to be stored in the data base.
A pretty printer is probably necessary to ensure that the front-end deals with
uniform data. Further, it might be decided to render some information in a specific
form (e.g. a table or diagram) and a pretty printer might help make such tasks easy.


```mermaid
graph LR
  Parser -- CPBInfo Object --> DataBase
  DataBase --> PrettyPrinter
  subgraph Read
    WebPage -- text --> Parser
  end
  subgraph Store
    DataBase
  end
  subgraph Retreive
    PrettyPrinter --> FrontEnd
  end
```
